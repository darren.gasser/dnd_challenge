use std::cmp;
use rand::distributions::{Distribution, Uniform};
use histogram::Histogram;
use futures::executor::block_on;

const TRIALS: u32 = 100000000;

async fn straight_roll() -> Histogram {
    let mut rng = rand::thread_rng();
    let d20 = Uniform::from(1..21);
    let mut total:u32 = 0;
    let mut h = Histogram::new();

    for _i in 1..TRIALS {
        let roll = d20.sample(&mut rng);
        total += roll;
        assert!(h.increment(roll as u64).is_ok());
    }
    println!("Average of {} straight rolls = {1:.2}", TRIALS, f64::from(total) / f64::from(TRIALS));
    h
}

async fn advantage_of_disadvantage() -> Histogram {
    let mut rng = rand::thread_rng();
    let d20 = Uniform::from(1..21);
    let mut total:u32 = 0;
    let mut h = Histogram::new();

    for _i in 1..TRIALS {
        let r1 = d20.sample(&mut rng);
        let r2 = d20.sample(&mut rng);
        let r3 = d20.sample(&mut rng);
        let r4 = d20.sample(&mut rng);
        let roll = cmp::max(cmp::min(r1,r2), cmp::min(r3,r4));
        total += roll;
        assert!(h.increment(roll as u64).is_ok());
    }
    println!("Average of {} AoD rolls = {1:.2}", TRIALS, f64::from(total) / f64::from(TRIALS));
    h
}

async fn disadvantage_of_advantage() -> Histogram {
    let mut rng = rand::thread_rng();
    let d20 = Uniform::from(1..21);
    let mut total:u32 = 0;
    let mut h = Histogram::new();

    for _i in 1..TRIALS {
        let r1 = d20.sample(&mut rng);
        let r2 = d20.sample(&mut rng);
        let r3 = d20.sample(&mut rng);
        let r4 = d20.sample(&mut rng);
        let roll = cmp::min(cmp::max(r1,r2), cmp::max(r3,r4));
        total += roll;
        assert!(h.increment(roll as u64).is_ok());
    }
    println!("Average of {} DoA rolls = {1:.2}", TRIALS, f64::from(total) / f64::from(TRIALS));
    h
}

fn show_extra_credit(h: Histogram) {
  println!("Median {}",h.percentile(50.0).unwrap());
  println!("Chance of matching or beating a:");
  let mut total = 0;
  for i in (1..21).rev() {
    total += h.get(i).unwrap();
    println!("{} = {1:.2}%",i, (f64::from(total as u32) /  f64::from(TRIALS)) * 100.0);
  }
}

async fn roll_em() {
    let f1 = straight_roll();
    let f2 = advantage_of_disadvantage();
    let f3 = disadvantage_of_advantage();

    let (shist, aodhist, doahist) = futures::join!(f1,f2,f3);
    println!("Results of straight rolls:");
    show_extra_credit(shist);
    println!("Results of advantage-of-disadvantage rolls:");
    show_extra_credit(aodhist);
    println!("Results of disadvantage-of-advantage rolls:");
    show_extra_credit(doahist);
}

fn main() {
    block_on(roll_em());
}